var roleTable;

document.addEventListener('DOMContentLoaded', function () {
    var t = roleTable = $('#roleTable').DataTable({
        "aProcessing": true,
        "aServerSide": true,
        "ajax":{
            "url": " " + base_url + "roles/getRoles",
            "dataSrc": ""
        },
        "columns": [
            {"data": "id_role"},
            {"data": "nm_role"},
            {"data": "descrip"},
            {"data": "status"}
        ],
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": 0
        }],
        "order": [[1, 'asc']],
        "responsive": "true",
        "bDestroy": true,
        "iDisplayLenght": 10,
    });

    t.on('order.dt search.dt', function () {
        t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i){
            cell.innerHTML = i + 1;
            t.cell(cell).invalidate('dom')
        });
    }).draw();
});

//call database
$('#roleTable').DataTable();

//call modal
function openModal(){
    $('#modalRole').modal('show');
}