<?php

    // define("BASE_URL", "http://localhost/online_shop/");

    const BASE_URL = "http://localhost/online_shop/";

    //  Database Configuration
    const DB_HOST = "localhost";
    const DB_USER = "root";
    const DB_PASSWORD = "";
    const DB_NAME = "db_online_shop";
    const DB_CHARSET = "charset=utf8";

    // Decimal and thousand delimeters Ex. Rp.24.198,00
    const SPD = ",";
    const SPM = ".";

    // Currency Symbol
    const SMONEY = "Rp";

    // Time Zone
    date_default_timezone_set('Asia/Makassar');


    