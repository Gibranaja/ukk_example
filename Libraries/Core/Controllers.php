<?php

    class Controllers
    {
        public function __construct()
        {
            
            $this->views = new Views();
            // invoke method loadmode() to be excetuted
            $this->loadModel();
        }

        //load model class
        public function loadModel()
            {
                // get the name of the contrroller class and concatenate with word "model"
                //to get model of controller like "homemodel"
                $model = get_class($this)."Model";

                // refer to the modes folders
                $routeClass = "Models/".$model.".php";

                // check if the model exists
                // if it exitsx call the once & instance teh pmodel
                if(file_exists($routeClass))
                {
                    require_once($routeClass);
                    $this->model = new $model();
                }
            }
        
    }
?>