<?php

    class Mysql extends Connection
    {
        private $myConn;
        private $strQuery;
        private $arrData;


        function __construct()
        {
            $this->myConn = new Connection();
            $this->myConn = $this->myConn->connect();
        }

        // insert method
        public function insert(string $query, array $arrValues)
        {
            $this->strQuery = $query;
            $this->arrData = $arrValues;

            $insert = $this->myConn->prepare($this->strQuery);
            $res = $insert->execute($this->arrData);

            if($res){
                $lastInsert = $this->myConn->lastInsertId();
            }else{
                $lastInsert = 0;
            }

            return $lastInsert;
        }

        // Select one spesific data
        public function select(string $query)
        {
            $this->strQuery = $query;

            $res = $this->myConn->prepare($this->strQuery);
            $res->execute();
            $data = $res->fetch(PDO::FETCH_ASSOC);
            return $data;
        }

        // Select All Data
        public function select_all(string $query)
        {
            $this->strQuery = $query;

            $res = $this->myConn->prepare($this->strQuery);
            $res->execute();
            $data = $res->fetchall(PDO::FETCH_ASSOC);
            return $data;
        }

        // Update data
        public function update(string $query, array $arrValues)
        {
            $this->strQuery = $query;
            $this->arrData = $arrValues;

            $update = $this->myConn->prepare($this->strQuery);
            $resExecute = $update->execute($this->arrData);
            return $resExecute;
        }

        public function delete(string $query)
        {
            $this->strQuery = $query;

            $res = $this->myConn->prepare($this->strQuery);
            $res->execute();
            return $res;
        }
    }