<?php
    class Product extends Controllers{
        public function __construct()
        {
            parent::__construct();
        }
        public function product($params)
        {
            $data['tag_page'] = "Product";
            $data['page_title'] = "Daftar Produk";
            $data['page_name'] = "product";
            $this->views->getView($this, "product", $data);

        }
        
    }
?>