<?php

    class Roles extends Controllers{

        public function __construct()
        {
            parent::__construct();
        }

        public function roles()
        {
            $data['page_id'] = 3;
            $data['page_title'] = "User Role | Admin";
            $data['page_name'] = "User Role";
            $data['page_tag'] = "user_role";

            $this->views->getView($this, "roles", $data);
        }

        // get all data form table rows
        public function getRoles()
        {
            $arrData = $this->model->selectRoles();

            for($i = 0; $i < count($arrData); $i++)
            {
                if($arrData[$i]['status'] == 1)
                {
                    $arrData[$i]['status'] = '<span class="badge badge-success">Active</span>';
                }else{
                    $arrData[$i]['status'] = '<span class="badge badge-danger">InActive</span>';
                }
            }

            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            die();
        }
    }