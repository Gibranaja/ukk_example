<?php

    class Dashboard extends Controllers{
        public function __construct()
        {
            parent::__construct();
        }
        public function dashboard($params)
        {
            $data['tag_page'] = "Dashboard Admin";
            $data['page_title'] = "Dashboard Admin";
            $data['page_name'] = "dashboard";
            $this->views->getView($this, "dashboard", $data);

        }
        
    }
?>