<?php 
    function base_url()
    {
        return BASE_URL;
    }
    function assetAdmin()
    {
        return BASE_URL."Assets/admin/";
    }
    function headerAdmin($data="")
    {
        $view_header = "Views/layouts/header_admin.php";
        require_once($view_header);
    }
    function footerAdmin()
    {
        $view_footer =  "Views/layouts/footer_admin.php";
        require_once($view_footer);
    }

    function getModal(string $nameModal, $data)
    {
        $view_modal = "Views/layouts/modals/{$nameModal}.php";
        require_once($view_modal);
    }

    // Remove excess space between words
    function strClean($strChar){
        $string = preg_replace(['/\s+/','/^\s|\s$/'],[' ',''], $strChar);
        $string = trim($string); //Remove leading and trailing whitespace
        $string = stripslashes($string); // Remove the inverted \
        $string = str_ireplace("<script>","",$string);
        $string = str_ireplace("</script>","",$string);
        $string = str_ireplace("<script src>","",$string);
        $string = str_ireplace("<script type=>","",$string);
        $string = str_ireplace("SELECT * FROM","",$string);
        $string = str_ireplace("DELETE FROM","",$string);
        $string = str_ireplace("INSERT INTO","",$string);
        $string = str_ireplace("SELECT COUNT(*) FROM","",$string);
        $string = str_ireplace("DROP TABLE","",$string);
        $string = str_ireplace("OR '1'='1","",$string);
        $string = str_ireplace('OR "1"="1"',"",$string);
        $string = str_ireplace('OR ´1´=´1´',"",$string);
        $string = str_ireplace("is NULL; --","",$string);
        $string = str_ireplace("is NULL; --","",$string);
        $string = str_ireplace("LIKE '","",$string);
        $string = str_ireplace('LIKE "',"",$string);
        $string = str_ireplace("LIKE ´","",$string);
        $string = str_ireplace("OR 'a'='a","",$string);
        $string = str_ireplace('OR "a"="a',"",$string);
        $string = str_ireplace("OR ´a´=´a","",$string);
        $string = str_ireplace("OR ´a´=´a","",$string);
        $string = str_ireplace("--","",$string);
        $string = str_ireplace("^","",$string);
        $string = str_ireplace("[","",$string);
        $string = str_ireplace("]","",$string);
        $string = str_ireplace("==","",$string);
        return $string;
    }

    // Generate a 10 charachteer password
    function passGenerator($length = 10)
    {
        $pass = "";
        $lengthPass=$length;
        $charKey = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijlkmnopqrstuvwxyz1234567890";
        $lengthCharKey=strlen($charKey);

        for($i=1; $i<=$lengthPass; $i++)
        {
            $randNum = rand(0,$lengthCharKey-1);
            $pass .= substr($charKey,$randNum,1);
        }
        return $pass;
    }

    // Generate Token
    function token()
    {
        $r1 = bin2hex(random_bytes(10));
        $r2 = bin2hex(random_bytes(10));
        $r3 = bin2hex(random_bytes(10));
        $r4 = bin2hex(random_bytes(10));
        $token = $r1.'-'.$r2.'-'.$r3.'-'.$r4;
        return $token;
    }

    // Money Format
    function formatMoney($amount){
        $amount = number_format($amount,2,SPD,SPM);
        return $amount;
    }
?>