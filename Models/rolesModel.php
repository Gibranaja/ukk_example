<?php

    class rolesModel extends Mysql
    {
        public function __construct()
        {
            parent::__construct();
        }

        // method to select data from role
        public function selectRoles()
        {
            $sql = "SELECT * FROM tb_role WHERE status !=0";
            $request = $this->select_all($sql);
            return $request;
        }
    }
?>
