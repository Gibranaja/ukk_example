<!-- Modal Dialog -->
<div class="modal fade" id="modalRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">New Role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> <!-- Modal Header -->
      
      <div class="modal-body">
        <div class="tile">
                
                <div class="tile-body">
                <form>
                    <div class="form-group">
                    <label class="control-label">Name</label>
                    <input class="form-control" id="role_name" name="role_name" type="text" placeholder="Role Name" required>
                    </div>
                    <div class="form-group">
                    <label class="control-label">Description</label>
                    <textarea class="form-control" rows="2" id="role_dscp" name="role_dscp" placeholder="Role Descprition"></textarea>
                    </div>
                    <div class="form-group">
                    <label for="role_status">Status</label>
                    <select class="form-control" name="role_status" id="role_status" required>
                        <option value="1">Active</option>
                        <option value="">InActive</option>
                    </select>
                    </div>
                    <div class="tile-footer">
                        <button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                    </div>
                    
                </form>
                </div>
                
            </div>
        </div>
      </div> <!-- Modal Body -->
      
      
      
    </div> <!-- Modal Content -->
    
  </div>
</div>