    <!-- Base URL For JavaScript -->
    <script>
      const base_url = "<?= base_url(); ?>";
    </script>
    
    <!-- Essential javascripts for application to work-->
    <script src="<?= assetAdmin(); ?>js/jquery-3.3.1.min.js"></script>
    <script src="<?= assetAdmin(); ?>js/popper.min.js"></script>
    <script src="<?= assetAdmin(); ?>js/bootstrap.min.js"></script>
    <script src="<?= assetAdmin(); ?>js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="<?= assetAdmin(); ?>js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="<?= assetAdmin(); ?>/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?= assetAdmin(); ?>/js/plugins/dataTables.bootstrap.min.js"></script>

    <script src="<?= assetAdmin(); ?>/js/custom.js"></script>
    
    <!-- Page specific javascripts-->
    <script src="<?= assetAdmin(); ?>js/font-awesome.js"></script>
  </body>
</html>