<!doctype html>
<html lang="en">
  <head>
      <!-- fontaweosme -->
      <link href="assets/img/workspace.png" rel="icon">
      <script src="https://kit.fontawesome.com/05a6280d63.js" crossorigin="anonymous"></script>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
      <!-- navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">
        <img src="<?php echo base_url(); ?>assets/images/furnitures.png" alt="Logo" style="width:50px;">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="<?= base_url();?>Home">Beranda <span class="sr-only">(current)</span></a>
        </li>
        <!-- <li class="nav-item">
            <a class="nav-link" href="#">Produk Kami</a>
        </li> -->
        
        </ul>
        <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
    </nav>

    <!-- slider -->
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="assets/images/slider/logo-0.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
            <img src="assets/images/slider/logo-1.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
            <img src="assets/images/slider/logo-2.jpg" class="d-block w-100" alt="...">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </button>
        </div>


        <!-- card -->
        <section id="services" class="services section-bg">
        <div class="container my-5">

            <div class="section-title my-5" data-aos="fade-up">
            <h2 style="text-align:center;">Produk Kami</h2>
            </div>

            <div class="row">
            <div class="col-4">
                <div class="card">
                    <img src="assets/images/products/chair-0.jpg" class="card-img-top" alt="some">
                    <div class="card-body">
                        <p class="card-text">Sofa Glostad Blue</p>
                        <p style="color: orange;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star-half-alt"></i>
                            <i class="far fa-star"></i>
                        </p>
                        <a href="#" class="btn btn-primary">Rp. 399.000</a>
                        <a href="#" class="btn btn-success">BELI</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <img src="assets/images/products/sofa-1.jpg" class="card-img-top" alt="some">
                    <div class="card-body">
                        <p class="card-text">Sofa Glostad Black</p>
                        <p style="color: orange;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star-half-alt"></i>
                            <i class="far fa-star"></i>
                        </p>
                        <a href="#" class="btn btn-primary">Rp. 599.000</a>
                        <a href="#" class="btn btn-success">BELI</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <img src="assets/images/products/sofa-2.jpg" class="card-img-top" alt="some">
                    <div class="card-body">
                        <p class="card-text">Sofa Glostad Blue</p>
                        <p style="color: orange;">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star-half-alt"></i>
                            <i class="far fa-star"></i>
                        </p>
                        <a href="#" class="btn btn-primary">Rp. 499.000</a>
                        <a href="#" class="btn btn-success">BELI</a>
                    </div>
                </div>
            </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

    
  </body>
</html>